# Дивиденды публичных компаний

[![pipeline status](https://gitlab.com/Serganbus/stock-dividends/badges/main/pipeline.svg)](https://gitlab.com/Serganbus/stock-dividends/-/commits/main)

## Описание данных
json-schema для описания файла с дивидендами компании: [dividends.schema](https://gitlab.com/Serganbus/stock-dividends/-/blob/main/dividends.schema.json)
